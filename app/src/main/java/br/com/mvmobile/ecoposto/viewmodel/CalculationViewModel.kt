package br.com.mvmobile.ecoposto.viewmodel

import android.databinding.ObservableField

class CalculationViewModel {

    var ethanol = ""
    var gasoline = ""
    var result = ObservableField("")

    fun calculate() {
        when {
            ethanol.isEmpty() -> setResult("Ethanol is invalid")
            gasoline.isEmpty() -> setResult("Gasoline is invalid")

            else -> {
                val calc = ethanol.toFloat() / gasoline.toFloat()

                when {
                    calc > 0.7 -> setResult("Gasoline")
                    calc <= 0.7 -> setResult("Ethanol")
                }
            }
        }
    }

    private fun setResult(result: String) {
        this.result.set(result)
    }

}