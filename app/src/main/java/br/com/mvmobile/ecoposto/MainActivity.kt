package br.com.mvmobile.ecoposto

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import br.com.mvmobile.ecoposto.databinding.ActivityMainBinding
import br.com.mvmobile.ecoposto.viewmodel.CalculationViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        //To be injected
        binding.viewModel = CalculationViewModel()
    }

}
