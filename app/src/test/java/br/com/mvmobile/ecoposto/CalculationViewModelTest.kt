package br.com.mvmobile.ecoposto

import br.com.mvmobile.ecoposto.viewmodel.CalculationViewModel
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class CalculationViewModelTest {

    lateinit var viewModel: CalculationViewModel

    @Before
    fun setup() {
        viewModel = CalculationViewModel()
    }

    @Test
    fun `result is ethanol`() {
        viewModel.ethanol = "2.099"
        viewModel.gasoline = "3.699"
        viewModel.calculate()

        assertEquals(viewModel.result.get(), "Ethanol")
    }

    @Test
    fun `result is gasoline`() {
        viewModel.ethanol = "3.099"
        viewModel.gasoline = "3.699"
        viewModel.calculate()

        assertEquals(viewModel.result.get(), "Gasoline")
    }

    @Test
    fun `ethanol is invalid`() {
        viewModel.ethanol = ""
        viewModel.gasoline = "3.699"
        viewModel.calculate()

        assertEquals(viewModel.result.get(), "Ethanol is invalid")
    }

    @Test
    fun `gasoline is invalid`() {
        viewModel.ethanol = "2.099"
        viewModel.gasoline = ""
        viewModel.calculate()

        assertEquals(viewModel.result.get(), "Gasoline is invalid")
    }

}