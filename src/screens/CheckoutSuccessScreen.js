import React from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity
} from 'react-native';
import colors from '../styles/colors';

export default class CheckoutSuccessScreen extends React.PureComponent {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);

        this.state = {
            infoMessage: 'Thank you for your order!'
        };
    }

    render() {
        return (
            <TouchableOpacity
                style={styles.container}
                onPress={() => this.props.navigation.navigate('Menu')}
            >
                <Text style={styles.infoMessage}>
                    {this.state.infoMessage}
                </Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white,
        justifyContent: 'center',
        alignItems: 'center',
    },
    infoMessage: {
        fontSize: 34,
        fontStyle: 'italic',
        color: colors.flamePea,
        letterSpacing: -0.82,

        paddingHorizontal: 80,
        textAlign: 'center',
    },
});
  