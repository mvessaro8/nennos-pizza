import React from 'react';
import {
    AsyncStorage,
    FlatList,
    StyleSheet,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View
} from 'react-native';
import api from '../services/api';
import colors from '../styles/colors';
import general from '../styles/general';

export default class CustomPizzaScreen extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            basePrice: 0,
            ingredients: [],
            selectedIngredients: []
        };
    }

    async componentDidMount() {
        const { navigation } = this.props;
        const basePrice = navigation.getParam('basePrice', 0);

        this.setState({ basePrice: basePrice });

        this.ingredients();
    }

    ingredients = async () => {
        const response = await api.get("/ozt3z");
        this.setState({ ingredients: response.data });
    };

    _renderHeader = () => {
        return (
            <Text style={styles.header}>
                Ingredients
            </Text>
        );
    };

    selectIngredient(item) {
        const selecteds = [...this.state.selectedIngredients];

        if (selecteds.some(ingredient => ingredient.id === item.id)) {
            this.setState({
                selectedIngredients: selecteds.filter(ingredient => ingredient.id !== item.id)
            });

        } else {
            selecteds.push(item);
            this.setState({ selectedIngredients: selecteds });
        }
    }

    _renderItem = ({ item }) => (
        <View>
            <TouchableOpacity
                style={styles.contentItem}
                onPress={() => this.selectIngredient(item)}
            >
                <Text style={styles.itemName}>
                    {item.name}
                </Text>
                <Text style={styles.itemPrice}>
                    ${item.price}
                </Text>
            </TouchableOpacity>
        </View>
    );

    _renderSeparator = () => (
        <View style={styles.separator}/>
    );

    priceCustomPizza() {
        return this.state.selectedIngredients
            .map(item => item.price)
            .reduce((acc, elem) => acc + elem, this.state.basePrice);
    }

    async _addToCart() {
        try {
            const restoredCart = await AsyncStorage.getItem('@NennosPizza:cart');
            const cart = JSON.parse(restoredCart) || [];

            cart.push({
                key: 'pizza',
                value: {
                    name: 'Custom pizza',
                    ingredients: this.state.selectedIngredients
                        .map(item => item.id),
                },
                price: this.priceCustomPizza(),
            });

            await AsyncStorage.setItem('@NennosPizza:cart', JSON.stringify(cart));

            ToastAndroid.show('ADDED TO CART', ToastAndroid.SHORT);

        } catch (error) {
            ToastAndroid.show('Error saving data - custom pizza', ToastAndroid.SHORT);
        }
    }

    render() {
        const ingredientsSortByName = this.state.ingredients
            .sort((a, b) => {
                if (a.name.toLowerCase() < b.name.toLowerCase()) {
                    return -1;
                }

                if (a.name.toLowerCase() > b.name.toLowerCase()) {
                    return 1;
                }

                return 0;
            });

        return (
            <View style={styles.container}>
                <FlatList
                    data={ingredientsSortByName}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    ListHeaderComponent={this._renderHeader}
                    ItemSeparatorComponent={this._renderSeparator}
                />
                <TouchableOpacity
                    style={styles.addCartContainer}
                    onPress={() => this._addToCart()}
                    disabled={this.state.selectedIngredients.length < 1}
                >
                    <Text style={styles.addCartText}>
                        {`Add to cart ($${this.priceCustomPizza()})`.toUpperCase()}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    ...general,

    container: {
      flex: 1,
      backgroundColor: colors.white,
    },
    header: {
        padding: 12,
        fontSize: 24,
        color: colors.tundora,
        letterSpacing: -0.58,
    },
    contentItem: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    addCartContainer: {
        backgroundColor: colors.sunglow,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    addCartText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: colors.white,
        letterSpacing: -0.39
    },
});
  