import React from 'react';
import {
    AsyncStorage,
    Button,
    FlatList,
    StyleSheet,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View
} from 'react-native';
import api from '../services/api';
import colors from '../styles/colors';
import general from '../styles/general';

export default class CartScreen extends React.PureComponent {

    static navigationOptions = ({ navigation }) => {
        return {
          headerRight: (
              <Button
                  onPress={() => navigation.navigate('Drink')}
                  title="drink"
              />
          ),
        }
      };

    constructor(props) {
        super(props);

        this.state = {
            cart: []
        };
    }

    async componentDidMount() {
        await this.cart();
    }

    async cart() {
        try {
            const restoredCart = await AsyncStorage.getItem('@NennosPizza:cart');
            const cart = JSON.parse(restoredCart) || [];

            this.setState({ cart: cart });

        } catch (error) {
            ToastAndroid.show('Error restore data - cart', ToastAndroid.SHORT);
        }
    };

    async removeItem(item) {
        const cart = [...this.state.cart];
        cart.splice(cart.indexOf(item), 1);

        await this._saveCart(cart);
    }

    async _saveCart(cart) {
        try {
            await AsyncStorage.setItem('@NennosPizza:cart', JSON.stringify(cart));
            this.setState({ cart: cart });
            
        } catch (error) {
            ToastAndroid.show('Error saving data - cart', ToastAndroid.SHORT);
        }
    }

    _renderItem = ({ item }) => (
        <View>
            <TouchableOpacity
                style={styles.contentItem}
                onPress={() => this.removeItem(item)}
            >
                <View style={styles.itemContainer}>
                    <Text style={styles.itemIcon}>
                        -
                    </Text>
                    <Text style={[styles.itemName, {
                        marginLeft: 10,
                    }]}>
                        {item.value.name}
                    </Text>
                </View>

                <Text style={styles.itemPrice}>
                    ${item.price}
                </Text>
            </TouchableOpacity>
        </View>
    );

    _renderSeparator = () => (
        <View style={styles.separator}/>
    );

    _renderFooter = () => (
        <View style={[styles.contentItem, {
            paddingTop: 12,
        }]}>
            <Text style={[styles.itemName, {
                fontWeight: 'bold',
            }]}>
                TOTAL
            </Text>
            <Text style={[styles.itemPrice, {
                fontWeight: 'bold',
            }]}>
                ${this._cartPrice()}
            </Text>
        </View>
    );

    _cartPrice() {
        return this.state.cart
            .map(item => item.price)
            .reduce((acc, elem) => acc + elem, 0);
    }

    async _proceedToCheckout() {
        const data = {
            pizzas: this.state.cart
                .filter(item => item.key === 'pizza')
                .map(item => item.value),
                
            drinks: this.state.cart
                .filter(item => item.key === 'drink')
                .map(item => item.value.id),
        };

        await api.setBaseURL('http://ptsv2.com/t/xansx-1538714141');
        const response = await api.post('/post', data);

        if (response.ok) {
            await api.setBaseURL('https://api.myjson.com/bins');

            await this._saveCart([]);
            this.props.navigation.navigate('CheckoutSuccess');

        } else {
            ToastAndroid.show('Error proceed to checkout', ToastAndroid.SHORT);
        }
    }

    render() {
        const cartSortByName = this.state.cart
            .sort((a, b) => {
                if (a.value.name.toLowerCase() < b.value.name.toLowerCase()) {
                    return -1;
                }

                if (a.value.name.toLowerCase() > b.value.name.toLowerCase()) {
                    return 1;
                }

                return 0;
            });

        return (
            <View style={styles.container}>
                <FlatList
                    data={cartSortByName}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={this._renderSeparator}
                    ListFooterComponent={this._renderFooter}
                />
                <TouchableOpacity
                    style={styles.checkOutContainer}
                    onPress={() => this._proceedToCheckout()}
                    disabled={this.state.cart.length < 1}
                >
                    <Text style={styles.checkOutText}>
                        {`Checkout ($${this._cartPrice()})`.toUpperCase()}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    ...general,

    container: {
      flex: 1,
      backgroundColor: colors.white,
    },
    contentItem: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    checkOutContainer: {
        backgroundColor: colors.cinnabar,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    checkOutText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: colors.white,
        letterSpacing: -0.39
    },
});
  