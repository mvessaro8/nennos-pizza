import React from 'react';
import {
    AsyncStorage,
    FlatList,
    StyleSheet,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View
} from 'react-native';
import api from '../services/api';
import colors from '../styles/colors';
import general from '../styles/general';

export default class DrinkScreen extends React.PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            drinks: []
        };
    }

    async componentDidMount() {
        this.drinks();
    }

    drinks = async () => {
        const response = await api.get("/150da7");
        this.setState({ drinks: response.data });
    };

    async _addDrink(item) {
        try {
            const restoredCart = await AsyncStorage.getItem('@NennosPizza:cart');
            const cart = JSON.parse(restoredCart) || [];

            cart.push({
                key: 'drink',
                value: {
                    id: item.id,
                    name: item.name
                },
                price: item.price,
            });

            await AsyncStorage.setItem('@NennosPizza:cart', JSON.stringify(cart));

            ToastAndroid.show('ADDED TO CART', ToastAndroid.SHORT);

        } catch (error) {
            ToastAndroid.show('Error saving data - drink', ToastAndroid.SHORT);
        }
    }

    _renderItem = ({ item }) => (
        <View>
            <TouchableOpacity
                style={styles.contentItem}
                onPress={() => this._addDrink(item)}
            >
                <View style={styles.itemContainer}>
                    <Text style={styles.itemIcon}>
                        +
                    </Text>
                    <Text style={[styles.itemName, {
                        marginLeft: 5,
                    }]}>
                        {item.name}
                    </Text>
                </View>

                <Text style={styles.itemPrice}>
                    ${item.price}
                </Text>
            </TouchableOpacity>
        </View>
    );

    _renderSeparator = () => (
        <View style={styles.separator} />
    );

    render() {
        const drinksSortByName = this.state.drinks
            .sort((a, b) => {
                if (a.name.toLowerCase() < b.name.toLowerCase()) {
                    return -1;
                }

                if (a.name.toLowerCase() > b.name.toLowerCase()) {
                    return 1;
                }

                return 0;
            });

        return (
            <View style={styles.container}>
                <FlatList
                    data={drinksSortByName}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                    ItemSeparatorComponent={this._renderSeparator}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    ...general,

    container: {
        flex: 1,
        backgroundColor: colors.white,
    },
    contentItem: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
});
