import React from 'react';
import {
    AsyncStorage,
    Button,
    FlatList,
    Image,
    StyleSheet,
    Text,
    ToastAndroid,
    TouchableOpacity,
    View
} from 'react-native';
import api from '../services/api';
import colors from '../styles/colors';

export default class MenuScreen extends React.PureComponent {

    static navigationOptions = ({ navigation }) => {
      return {
        headerLeft: (
            <Button
                onPress={() => navigation.navigate('Cart')}
                title="cart"
            />
        ),
        headerRight: (
            <Button
                onPress={() => navigation.navigate('CustomPizza', {
                  basePrice: navigation.getParam('basePrice')
                })}
                title="custom"
            />
        ),
      }
    };

    constructor(props) {
        super(props);

        this.state = {
          basePrice: 0,
          pizzas: [],
          ingredients: []
        };
    }

    async componentDidMount() {
        await this.ingredients();
        await this.pizzas();
    }

    ingredients = async () => {
        const response = await api.get("/ozt3z");
        this.setState({ ingredients: response.data });
    };

    pizzas = async () => {
        const response = await api.get("/dokm7");
        const pizzas = response.data.pizzas;

        this.setState({
          basePrice: response.data.basePrice,
          pizzas: pizzas
        });

        this.props.navigation.setParams({ basePrice: this.state.basePrice });
    }

    nameOfIngredients(ingredientsIds) {
        const ingredients = this.state.ingredients;
        let nameOfIngredients = '';

        ingredientsIds.forEach(function (item, index, array) {
            const ingredient = ingredients.find((i) => i.id === item);

            if (index !== array.length - 1) {
                nameOfIngredients += ingredient.name + ', ';
            } else {
                nameOfIngredients += ingredient.name + '.';
            }
        });

        return nameOfIngredients;
    }

    price(pizza) {
        const ingredientsOfPizza = pizza.ingredients;

        const priceOfIngredients = this.state.ingredients
            .filter(item => ingredientsOfPizza.some(i => i === item.id))
            .map(item => item.price)
            .reduce((acc, elem) => acc + elem, 0);

        return this.state.basePrice + priceOfIngredients;
    }

    async _addToCart(item) {
        try {
            const restoredCart = await AsyncStorage.getItem('@NennosPizza:cart');
            const cart = JSON.parse(restoredCart) || [];

            cart.push({
                key: 'pizza',
                value: {
                    name: item.name,
                    ingredients: item.ingredients,
                },
                price: this.price(item),
            });

            await AsyncStorage.setItem('@NennosPizza:cart', JSON.stringify(cart));

            ToastAndroid.show('ADDED TO CART', ToastAndroid.SHORT);

        } catch (error) {
            ToastAndroid.show('Error saving data - menu', ToastAndroid.SHORT);
        }
    }

    _renderItem = ({ item }) => (
        <View style={styles.itemContainer}>
            <Image
                style={styles.itemImageUrl}
                source={{ uri: item.imageUrl }}
            />

            <View style={styles.itemTextContainer}>
                <Text style={styles.textNamePizza}>
                    {item.name}
                </Text>
                <Text style={styles.textIngredients}>
                    {this.nameOfIngredients(item.ingredients)}
                </Text>
            </View>

            <TouchableOpacity
                style={styles.priceContainer}
                onPress={() => this._addToCart(item)}
            >
                <Text style={styles.price}>
                    ${this.price(item)}
                </Text>
            </TouchableOpacity>
        </View>
    );

    render() {
        return (
            <View style={styles.container}>
                <FlatList
                    data={this.state.pizzas}
                    renderItem={this._renderItem}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    item: {
      padding: 10,
      fontSize: 18,
      height: 'auto',
    },

    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        minHeight: 85,
        backgroundColor: colors.alabaster,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemImageUrl: {
        width: 75,
        height: 75
    },
    itemTextContainer: {
        flex: 1,
        flexDirection: 'column',
        paddingVertical: 12,
        paddingLeft: 12
    },
    textNamePizza: {
        fontSize: 24,
        fontWeight: 'bold',
        color: colors.tundora,
        letterSpacing: -0.58,
    },
    textIngredients: {
        fontSize: 14,
        color: colors.tundora,
        letterSpacing: -0.34,
    },
    priceContainer: {
        backgroundColor: colors.sunglow,
        borderRadius: 4,
        height: 30,
        width: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 12,
    },
    price: {
        fontSize: 16,
        fontWeight: 'bold',
        color: colors.white,
        letterSpacing: -0.39,
    },
});
