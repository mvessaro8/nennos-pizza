import React from 'react';
import { createStackNavigator } from "react-navigation";
import MenuScreen from "./screens/MenuScreen";
import CustomPizzaScreen from './screens/CustomPizzaScreen';
import DrinkScreen from './screens/DrinkScreen';
import CartScreen from './screens/CartScreen';
import CheckoutSuccessScreen from './screens/CheckoutSuccessScreen';

const RootStack = createStackNavigator(
    {
        Menu: {
            screen: MenuScreen,
            navigationOptions: {
                title: "Nenno's Pizza"
            }
        },
        CustomPizza: {
            screen: CustomPizzaScreen,
            navigationOptions: {
                title: "Create a pizza"
            }
        },
        Drink: {
            screen: DrinkScreen,
            navigationOptions: {
                title: "Drinks"
            }
        },
        Cart: {
            screen: CartScreen,
            navigationOptions: {
                title: "Cart"
            }
        },
        CheckoutSuccess: {
            screen: CheckoutSuccessScreen
        }
    },
    {
        initialRouteName: 'Menu'
    }
);

export class App extends React.PureComponent {
    render() {
        return <RootStack />;
    }
}