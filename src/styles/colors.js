const colors = {
    alabaster: '#F8F8F8',
    cinnabar: '#E14D45',
    frenchGray: '#C8C7CC',
    flamePea: '#DF4E4A',
    sunglow: '#FFCD2B',
    tundora: '#4A4A4A',
    white: '#FFFFFF',
};

export default colors;