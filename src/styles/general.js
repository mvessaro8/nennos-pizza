import colors from './colors';

const general = {
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    itemIcon: {
        color: colors.cinnabar,
        fontSize: 24,
        textAlign: 'center',
        alignItems: 'center',
        padding: 12,
    },
    itemName: {
        marginLeft: 43,
        paddingVertical: 12,
        fontSize: 17,
        height: 'auto',
        color: colors.tundora,
        letterSpacing: -0.41,
    },
    itemPrice: {
        padding: 12,
        fontSize: 17,
        height: 'auto',
        color: colors.tundora,
        letterSpacing: -0.41,
    },
    separator: {
        marginLeft: 40,
        height: .5,
        width: "auto",
        backgroundColor: colors.frenchGray,
    },
};

export default general;