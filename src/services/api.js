import { create } from 'apisauce';

const api = create({
    baseURL: 'https://api.myjson.com/bins'
});

export default api;